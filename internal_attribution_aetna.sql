/* 
=============================================
 Procedure name:     Attribution_DimMemberAttribution.sql

 Author:		Tim Thyne
 Create date:  10/19/2017
 Revised date: 
 File Lag: +1 month. Files received in current month are effective for the following month. Example, files received in January 2018 are for February membership. Effective date would be 2018-02-01.
 Description:
        This processes Aetna member attribution. It prioritizes claims data over enrollment data. The rules are as follows:
			1. 24 months expanded E&M codes, providers most recent date of service with at least two visits, CIN roster PCPs and all CIN roster mid-levels
			2. 24 months expanded E&M codes, providers most recent date of service, CIN roster PCPs and all CIN roster mid-levels
			3. 24 months expanded E&M codes, providers most recent date of service, CIN roster specialists
			4. 24 months expanded E&M codes, providers most recent date of service, all remaining providers
			5. Patient PCP Nomination (from payer)
			6. Payer attribution (from Payer)
			7. Member has Group assignment, but no PCP assignment
			8. Does not meet attribution criteria

		Tables
			a. Staged_Attribution_AetnaMembers
				i. Active members from ACO_Aetna.dbo.Dim_Patients
			b. Staged_Attribution_AetnaQualifyingClaims
				i. Claims for prior 24 months from ACO_Aetna.aetna.claims_data_file (DBP server)
			   ii. Contain at least one procedure code in SHA_Analytics.analytics.aco_atrb_proc_cd (Expanded E&M Codes List)
			c. DimMemberAttributionRules (static)
			d. DimMemberAttributionPractices (static)
			e. DimMemberAttributionProviders
				i. CINMasterList_Current
			   ii. DimMemberAttributionPractices
					1) ADC, ARC, CAPMED, LSCC, SAIMS
					2) Else Other CIN
			  iii. Non-facility records
			f. Staged_Attribution_Rules� - output for each rule
			g. Staged_Attribution - combined output from ruled
			h. DimMemberAttribution - Final Attribution output

		Functions
			a. Coalesce(Field1, Field2) = case when Field1 is null then Field2 end
			b. Cast(null as int) = define a null field as a specific data type (ex. when the tables will be unioned, requiring uniform data types)
			c. Row_number() = returns the sequential number of a row within a partition of a resulting dataset, starting at 1 for the first row in each partition. 
			   Requires an over() clause containing at least an order by clause. Most often utilized with a partition by clause.
				i. Example: ProviderSequence = row_number() over(partition by MemberID order by LastDOS desc)
			   ii. Used to number the claim providers in order of most recent date-of-service given a pre-filtered list of qualifying claims
			  iii. When using row_number() in a subquery, you and select where row_number() field = 1 in the parent query to grab just the first row for each member (ex. Grab most recent provider per member in a pre-filtered list of claims)

		Process Steps:
			a. Update non-static dependencies
				i. Refresh DimMemberAttributionProviders
				ii. Refresh Staged_Attribution_AetnaMembers
					1) Patient is active
					2) Is current version
				iii. Refresh Staged_Attribution_AetnaQualifyingClaims
					1) Claims are filtered by those that only contain expanded E&M codes
					2) Claims are then grouped by member_id, provider TIN, and provider NPI
						a) Claims table lists distinct:
							i) MemberID
							ii) ProviderID (concatenation of TIN and NPI)
							iii) ProviderTIN
							iv) ProviderNPI
							v) ProviderClaimCount (how many distinct claims did the provider have?)
							vi) LastDOS (last day that member saw the provider on this row)
			b. Process Rules 1 to 6
				i. Claims based rules: 
					1) When processing a claims based rule:
						a) Select all records with qualifying claims that meet the rule criteria
						b) Order them by provider with most recent DOS
						c) Select provider with most recent DOS for each member
						d) Exclude members that were attributed in previous rules.
					2) Rule 1: More than 1 Qualifying claim with an active CIN roster PCP
					3) Rule 2: 1 qualifying claims with an active CIN roster PCP
					4) Rule 3: 1 or more qualifying claims with an active CIN roster Specialist
					5) Rule 4: Evaluate all other members with qualifying claims
						a) Outcome 1: Out-of-network
						b) Outcome 2: Inactive CIN Provider
							i) Will initially be classed to "Inactive" attribution group
							ii) Will be re-evaluated in Rule 7
				ii. Enrollment based rules. For each rule, exclude member that were attributed in previous rules.
					1) Rule 5: Select from active members where the patient nominated TIN and NPI are not null
					2) Rule 6: Select from active members where patient nominated TIN is null and payor attributed TIN and NPI are not null
			c. Process remaining members
				i. Temp_unattributed_a - Members that had qualifying claims, had a valid TIN, but had a null NPI
				ii. Temp_unattributed_b - Active members that have either a valid Patient Elected TIN or NPI but not both
				iii. Temp_unattributed_c - Active members that have either a valid Payor Attributed TIN or NPI but not both
				iv. Temp_unattributed_d - All other active members (no qualifying claims, no patient election, no payor election)
				v. Combine all unattributed members into tim.staged_Attribution_RuleUnattributed. For each temp table, exclude member that were attributed in previous rules/temp tables.
			d. Combine all transient "staged_Attribution_Rule�" tables into a combined "staged_Attribution" table
			e. Process members matched with Inactive CIN providers from staged_Attribution table
				i. Update attribution group to either an Active TIN or Out-of-Network
				ii. Update RuleID and RuleDescription to rule 7 for members that matches in the previous update step
			f. Process unattributed patients from staged_Attribution table
				i. Update AttributionGroup to either and Active TIN or Out-of-Network
				ii. Update RuleID and RuleDescription to rule 8 for members that were classified to 'Unattributed' in the previous update step
			g. Assemble final DimMemberAttribution table
				i. Drop tim.DimMemberAttribution
				ii. Source: tim.staged_Attribution_AetnaMembers (to list all active members)
					1) Left Join: tim.staged_Attribution on member id's
						a) Left Join: tim.DimMemberAttributionProviders (to list associated provider info from CIN Roster) on provider TIN and NPI

		Process Outputs:
        Returns the list of active members, their attributed Practice Group, attributed provider id's, the attribution rule the matches to, and the rule description

======================================================================================================================================
*/

USE ANALYTICS_SHA

--HAVE A DATE SPAN OVERRIDE FOR MEMBERSHIP DATE??

declare @EffectiveMonth date = '2018-01-01'
declare @LoadDtm datetime = getdate()

declare @RunProviders int = 0  --Change to 1 to refresh tim.DimMemberAttributionProviders
declare @RunMembers int = 0  --Change to 1 to refresh tim.staged_Attribution_AetnaMembers
declare @RunClaims int = 0   --Change to 1 to refresh tim.staged_Attribution_AetnaClaims
declare @RunAllRules int = 0 --Change to 1 to run attribution rules
declare @RunRule1 int = 0
declare @RunRule2 int = 0
declare @RunRule3 int = 0
declare @RunRule4 int = 0
declare @RunRule5 int = 0
declare @RunRule6 int = 0
declare @RunRule7 int = 0
declare @RunRule8 int = 0
declare @Publish  int = 0
declare @Overwrite int= 0 

--###########################################################################################################
--REBUILD PROVIDER TABLE
--###########################################################################################################
--Reproduced some information from the CIN Roster and add fields relevant to Member Attribution. Ignore Facility CIN Roster entries.
--Calculated Fields:
	--ProviderGroupAbbreviation - If the TIN is one of our 5 practices then assign them an abbreviation, else assign as 'Other CIN'
	--ProviderAttributionLevel - use business rules to determine if the provider is a PCP, Mid-Level, Specialist. If the provider is inactive then assign them as 'Inactive' 

--NEED TO IGNORE PROVIDERS THAT HAVEN'T STARTED YET.
if @RunProviders = 1
begin

drop table tim.DimMemberAttributionProviders

select
a.[Last Name] + ', ' + a.[First Name] ProviderName
,a.NPI ProviderNPI
,a.[Provider Start] ProviderStart
,a.[Provider Term] ProviderTerm
,a.[Specialty 1] ProviderSpecialty
,ProviderGroupAbbreviation = case
	when b.PracticeTIN is null then 'Other CIN'
	else b.PracticeAbbreviation
	end
,coalesce(a.[Group DBA Name],a.[Group Legal Name]) ProviderGroup
,a.[Group DBA Name] ProviderGroupDBAName
,a.[Group Legal Name] ProviderGroupLegalName
,a.TIN ProviderTIN
,a.[CCN Location 1] ProviderCCN1
,a.[CCN Location 2] ProviderCCN2
,a.[CCN Location 3] ProviderCCN3
,a.[CCN Location 4] ProviderCCN4
,ProviderAttributionLevel = case
	when a.[Provider Term] is not null then 'Inactive' --NEED TO UPDATE
	when a.[Provider Role] = 'Hospitalist' and a.[Provider Type] like '%PAC Phys%' then 'PCP'
	when a.[Provider Role] like '%PCP%' then 'PCP'
	when a.[Provider Role] like '%Spec%' then 'Specialist'
	when a.[Provider Role] like '%Hosp%' then 'Specialist'
	when a.[Provider Role] in ('APN','PA') and a.[Practitioner Type] = 'Primary' then 'Mid-Level'
	when a.[Provider Role] in ('APN','PA') and a.[Practitioner Type] = 'Specialty' then 'Specialist'
	end
,a.[Provider Role] ProviderRole
,a.[Practitioner Type] ProviderType

/*
	Business rules for assigning Attribution Level Indicator:
	From CIN Roster, IF [Provider Role] = 'Hospitalist' THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'PCP' THEN 'PCP'
	From CIN Roster, IF [Provider Role] = 'Specialist' THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'APN' AND [Practitioner Type] = 'Primary' THEN 'Mid-Level'
	From CIN Roster, IF [Provider Role] = 'PA' AND [Practitioner Type] = 'Primary' THEN 'Mid-Level'
	From CIN Roster, IF [Provider Role] = 'APN' AND [Practitioner Type] = 'Specialty' THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'PA' AND [Practitioner Type] = 'Specialty' THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'Facility' THEN IGNORE (remove from attribution roster)
	From CIN Roster, IF [Provider Role] = 'Hospitalist APN' OR 'Hospitalist PA' THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'Hospitalist' AND [Provider Type] = 'PAC Physician Group' THEN 'PCP'
	From CIN Roster, IF [Provider Role] = 'PCP APN' THEN 'PCP'
	From CIN Roster, IF [Provider Role] = 'PCP PA' THEN 'PCP'
	From CIN Roster, IF [Provider Role] = 'Specialist APN'  THEN 'Specialist'
	From CIN Roster, IF [Provider Role] = 'Specialist PA'  THEN 'Specialist'
*/

into tim.DimMemberAttributionProviders

from
AHWACOTXPLADBP.ANALYTICS_SHA.dbo.CINMasterList_Current a

	left join tim.DimMemberAttributionPractices b on a.TIN=b.PracticeTIN

where
a.[Provider Role] not in ('Facility')

order by
a.[Provider Role]

create index idx_TINNPI on tim.DimMemberAttributionProviders (ProviderNPI,ProviderTIN)
create index idx_tin on tim.DimMemberAttributionProviders (ProviderTIN)
create index idx_npi on tim.DimMemberAttributionProviders (ProviderNPI)

end

--###########################################################################################################
--GENERATE/CLEAN MEMBER LIST (DIM_PATIENT DATA FROM ACO_AETNA DATABASE)
--###########################################################################################################
--Pull selective information about active members from enrollment data. This is the population that attribution will be run for. Invalid TIN or NPIs are assigned to null values.
--This dataset will be analyzed after claims based rules have run.

--HOW DO WE HANDLE NO PCP ELECTION IF PAYOR DROPS ELECTION IN A CERTAIN MONTH
--NEED TO BE FLEXIBLE TO PULL BASED ON EFFECTIVE MONTH (RETROACTIVE)

if @RunMembers = 1
begin

drop table tim.staged_Attribution_AetnaMembers

select distinct
a.member_id MemberID
,case when a.pcp_tax_id = '' then null else concat(ltrim(rtrim(a.pcp_tax_id)), ltrim(rtrim(a.pcp_npi))) end PatientNominatedPCP_ID
,case when a.pcp_tax_id = '' then null else ltrim(rtrim(a.pcp_tax_id)) end PatientNominatedPCP_TIN
,case when rtrim(ltrim(a.pcp_npi)) = 0 then null else ltrim(rtrim(a.pcp_npi)) end PatientNominatedPCP_NPI
,case when a.attr_prvdr_tax_id like '%***%' then null else concat(ltrim(rtrim(a.attr_prvdr_tax_id)), ltrim(rtrim(a.attr_prvdr_npi))) end PayorAttributedPCP_ID
,case when a.attr_prvdr_tax_id like '%***%' then null else ltrim(rtrim(a.attr_prvdr_tax_id)) end PayorAttributedPCP_TIN
,case when rtrim(ltrim(a.attr_prvdr_npi)) = 0 then null else ltrim(rtrim(a.attr_prvdr_npi)) end PayorAttributedPCP_NPI
,rtrim(ltrim(a.first_name)) PatientFirstName
,rtrim(ltrim(a.middle_name)) PatientMiddleName
,rtrim(ltrim(a.last_name)) PatientLastName
,rtrim(ltrim(a.gender)) PatientGender --STANDARDIZE/PUSH TO M/F/U VALUES
,cast(a.dob as date) PatientDOB

into tim.staged_Attribution_AetnaMembers

from
AHWACOTXPLADBP.ACO_Aetna.dbo.dim_Patients a

where --USE EFFECTIVE DATES NOT FLAGS
a.pt_currently_active like '%yes%'
and a.current_version like '%yes%'

end

--###########################################################################################################
--GENERATE/CLEAN LIST OF QUALIFYING CLAIMS
--###########################################################################################################
--This query returns unique member_id, provider id's, provider qualifying claim count, and last date of service from the Aetna claims file.
--A qualifying claim is a claim that contains an Expanded E&M Procedure Code (i.e. any procedure codes contained in the ACO_ATRB_PROC_CD table).
--The date range is a 24 months look back from the max date in the claims data.
--Only looks at claims for active members (from AHWACOTXPLADBP.ACO_Aetna.dbo.dim_Patients where pt_currently_active like '%yes%' and a.current_version like '%yes%') (tim.staged_Attribution_AetnaMembers above)

--Claims table - AHWACOTXPLADBP.ACO_Aetna.aetna.claims_data_file
--Member table - tim.staged_Attribution_AetnaMembers (active members only)

if @RunClaims = 1
begin

--Declaring variables here so code can run piece-meal
declare @EndDate date = (select max(convert(date,service_start_date)) from AHWACOTXPLADBP.aco_aetna.aetna.claims_data_file)
declare @StartDate date = dateadd(month,datediff(month,0,@EndDate)-24,0)

drop table tim.staged_Attribution_AetnaQualifingClaims

select distinct
'AETNA' ClaimPayor
,b.MemberID
,concat(ltrim(rtrim(a.srvg_prvdr__tax_id)),case when ltrim(rtrim(a.srvg_prvdr_npi)) = '0' then null else ltrim(rtrim(a.srvg_prvdr_npi)) end) ProviderID
,ltrim(rtrim(a.srvg_prvdr__tax_id)) as ProviderTIN
,case when ltrim(rtrim(a.srvg_prvdr_npi)) = '0' then null else ltrim(rtrim(a.srvg_prvdr_npi)) end as ProviderNPI
,count(distinct service_start_date) ProviderClaimCount
,min(cast(a.service_start_date as date)) as FirstDOS
,max(cast(a.service_start_date as date)) as LastDOS

into tim.staged_Attribution_AetnaQualifingClaims

from
AHWACOTXPLADBP.ACO_Aetna.aetna.claims_data_file a

	join tim.staged_Attribution_AetnaMembers b on a.dw_member_id=b.MemberID
	join SHA_Analytics.analytics.aco_atrb_proc_cd c on a.prcdr_cd=c.proc_cd

where
a.service_start_date between @StartDate and @EndDate

group by
b.MemberID
,concat(ltrim(rtrim(a.srvg_prvdr__tax_id)),case when ltrim(rtrim(a.srvg_prvdr_npi)) = '0' then null else ltrim(rtrim(a.srvg_prvdr_npi)) end)
,ltrim(rtrim(a.srvg_prvdr__tax_id))
,case when ltrim(rtrim(a.srvg_prvdr_npi)) = '0' then null else ltrim(rtrim(a.srvg_prvdr_npi)) end

order by 
b.MemberID

create index idx_TINNPI on tim.staged_Attribution_AetnaQualifingClaims (ProviderNPI,ProviderTIN)
create index idx_tin on tim.staged_Attribution_AetnaQualifingClaims (ProviderTIN)
create index idx_npi on tim.staged_Attribution_AetnaQualifingClaims (ProviderNPI)

end 

--59,268 rows
--0:03 minutes/seconds elapsed


--###########################################################################################################
--START PROCESSING ATTRIBUTION RULES
--###########################################################################################################
--***********************************************************************************************************
--PROCESS RULE 1 - CLAIMS BASED - last CIN PCP or Midlevel (active) seen with at least two qualifying visits
--***********************************************************************************************************
--Select members that have a provider claim count greater than 1, order the output by most recent DOS by physician, and assign a row number partitioned by member (1 = most recent provider for that member).
--Limit this list to only Active CIN PCPs and PCP Midlevels
--Take that output and only select the most recent provider for that member (ProviderSequence = 1)

--NEED TO REVISE ACTIVE PROVIDER CRITERIA: NULL/FUTURE TERM DATE, START DATE IN THE PAST

if @RunRule1 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule1

select * into tim.staged_Attribution_Rule1 from (

select distinct
ClaimPayor
,MemberID
,ProviderID
,a.ProviderTIN
,a.ProviderNPI
,FirstDOS
,LastDOS
,b.ProviderAttributionLevel
,b.ProviderGroupAbbreviation AttributionGroup
,ProviderClaimCount
,row_number() over(partition by MemberID order by LastDOS desc) ProviderSequence
,1 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 1) RuleDescription

from
tim.staged_Attribution_AetnaQualifingClaims a

	join tim.DimMemberAttributionProviders b on a.ProviderTIN=b.ProviderTIN and a.ProviderNPI=b.ProviderNPI and b.ProviderTerm is null

where
a.ProviderClaimCount > 1
and b.ProviderAttributionLevel in ('PCP','Mid-Level')

) a

where
ProviderSequence = 1

end


--***********************************************************************************************************
--PROCESS RULE 2 - CLAIMS BASED - last qualifying visit with active CIN PCP or Midlevel
--***********************************************************************************************************
--Select members from claims data that have a provider claim count equal to 1, order the output by most recent DOS by physician, and assign a row number partitioned by member (1 = most recent provider for that member).
--Limit this list to only Active CIN PCPs and PCP Midlevels
--Take that output and only select the most recent provider for that member (ProviderSequence = 1)
--Exclude members that exist in previous rule assignments (ie. MemberID not in (select MemberID from tim.staged_Attribution_Rule1))

if @RunRule2 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule2

select * into tim.staged_Attribution_Rule2 from (

select distinct
ClaimPayor
,MemberID
,ProviderID
,a.ProviderTIN
,a.ProviderNPI
,FirstDOS
,LastDOS
,b.ProviderAttributionLevel
,b.ProviderGroupAbbreviation AttributionGroup
,ProviderClaimCount
,row_number() over(partition by MemberID order by LastDOS desc) ProviderSequence
,2 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 2) RuleDescription

from
tim.staged_Attribution_AetnaQualifingClaims a

	join tim.DimMemberAttributionProviders b on a.ProviderTIN=b.ProviderTIN and a.ProviderNPI=b.ProviderNPI and b.ProviderTerm is null

where
a.ProviderClaimCount = 1
and b.ProviderAttributionLevel in ('PCP','Mid-Level')

) a

where ProviderSequence = 1
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)

end

--***********************************************************************************************************
--PROCESS RULE 3 - CLAIMS BASED - last qualifying visit with CIN specialist
--***********************************************************************************************************
--Select members from claims data, order the output by most recent DOS by physician, and assign a row number partitioned by member (1 = most recent provider for that member).
--Limit this list to only Active CIN Specialist and Specialist Midlevels ([Practitioner Type] like '%spec%')
--Take that output and only select the most recent provider for that member (ProviderSequence = 1)
--Exclude members that exist in previous rule assignments (ie. MemberID not in (select MemberID from tim.staged_Attribution_Rule1))

if @RunRule3 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule3

select * into tim.staged_Attribution_Rule3 from (

select distinct
ClaimPayor
,MemberID
,ProviderID
,a.ProviderTIN
,a.ProviderNPI
,FirstDOS
,LastDOS
,b.ProviderAttributionLevel
,b.ProviderGroupAbbreviation AttributionGroup
,ProviderClaimCount
,row_number() over(partition by memberid order by LastDOS desc) ProviderSequence
,3 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 3) RuleDescription

from
tim.staged_Attribution_AetnaQualifingClaims a

	join tim.DimMemberAttributionProviders b on a.ProviderTIN=b.ProviderTIN and a.ProviderNPI=b.ProviderNPI and b.ProviderTerm is null

where
b.ProviderAttributionLevel = 'Specialist'

) a 

where
ProviderSequence = 1
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)

end

--***********************************************************************************************************
--PROCESS RULE 4 - CLAIMS BASED - last qualifying visit with out of network provider
--***********************************************************************************************************
--Select members from claims data, order the output by most recent DOS by physician, and assign a row number partitioned by member (1 = most recent provider for that member).
--Do not limit by CIN Providers
--Take that output and only select the most recent provider for that member (ProviderSequence = 1)
--Exclude members that exist in previous rule assignments (ie. MemberID not in (select MemberID from tim.staged_Attribution_Rule1))

if @RunRule4 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule4

select * into tim.staged_Attribution_Rule4 from (

select distinct
ClaimPayor
,MemberID
,ProviderID
,a.ProviderTIN
,a.ProviderNPI
,FirstDOS
,LastDOS
,coalesce(coalesce(b.ProviderAttributionLevel,c.ProviderAttributionLevel),'Out of Network') ProviderAttributionLevel
,coalesce(coalesce(b.ProviderGroupAbbreviation,c.ProviderGroupAbbreviation), 'Out of Network') AttributionGroup
,ProviderClaimCount
,row_number() over(partition by memberid order by LastDOS desc) ProviderSequence
,4 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 4) RuleDescription

from
tim.staged_Attribution_AetnaQualifingClaims a

	left join tim.DimMemberAttributionProviders b on a.ProviderTIN=b.ProviderTIN and a.ProviderNPI=b.ProviderNPI and b.ProviderTerm is null
	left join tim.DimMemberAttributionProviders c on a.ProviderTIN=c.ProviderTIN and a.ProviderNPI=c.ProviderNPI and c.ProviderTerm is not null

where
a.ProviderNPI is not null

) a 

where
ProviderSequence = 1
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)

end


--***********************************************************************************************************
--PROCESS RULE 5 - ENROLLMENT BASED - PATIENT NOMINATION OF PCP
--***********************************************************************************************************
--Select members from ENROLLMENT data (ACO_Aetna.aetna.Dim_Patients) where patient has selected a PCP
--Exclude members that exist in previous rule assignments (ie. MemberID not in (select MemberID from tim.staged_Attribution_Rule1))

if @RunRule5 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule5

select distinct
'AETNA' ClaimPayor
,MemberID
,PatientNominatedPCP_ID
,PatientNominatedPCP_TIN
,PatientNominatedPCP_NPI
,cast(null as date) FirstDOS
,cast(null as date) LastDOS
,coalesce(coalesce(b.ProviderAttributionLevel,c.ProviderAttributionLevel),'Out of Network') ProviderAttributionLevel
,coalesce(coalesce(b.ProviderGroupAbbreviation,c.ProviderGroupAbbreviation), 'Out of Network') AttributionGroup
,null ProviderClaimCount
,1 ProviderSequence --0 or null
,5 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 5) RuleDescription

into tim.staged_Attribution_Rule5

from
tim.staged_Attribution_AetnaMembers a

	left join tim.DimMemberAttributionProviders b on a.PatientNominatedPCP_TIN=b.ProviderTIN and a.PatientNominatedPCP_NPI=b.ProviderNPI and b.ProviderTerm is null
	left join tim.DimMemberAttributionProviders c on a.PayorAttributedPCP_TIN=c.ProviderTIN and a.PayorAttributedPCP_NPI=c.ProviderNPI and c.ProviderTerm is not null

where
PatientNominatedPCP_ID is not null
and PatientNominatedPCP_NPI is not null
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)

end

--***********************************************************************************************************
--PROCESS RULE 6 - ENROLLMENT BASED - PAYER ATTRIBUTION OF PCP
--***********************************************************************************************************
--Select members from ENROLLMENT data (ACO_Aetna.aetna.Dim_Patients) where patient has not selected a PCP and Payor has assigned a PCP
--Exclude members that exist in previous rule assignments (ie. MemberID not in (select MemberID from tim.staged_Attribution_Rule1))

if @RunRule6 = 1 or @RunAllRules = 1
begin

drop table tim.staged_Attribution_Rule6

select distinct
'AETNA' ClaimPayor
,MemberID
,PayorAttributedPCP_ID
,PayorAttributedPCP_TIN
,PayorAttributedPCP_NPI
,cast(null as date) FirstDOS
,cast(null as date) LastDOS
,coalesce(coalesce(b.ProviderAttributionLevel,c.ProviderAttributionLevel),'Out of Network') ProviderAttributionLevel
,coalesce(coalesce(b.ProviderGroupAbbreviation,c.ProviderGroupAbbreviation), 'Out of Network') AttributionGroup
,null ProviderClaimCount
,1 ProviderSequence
,6 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 6) RuleDescription

into tim.staged_Attribution_Rule6

from
tim.staged_Attribution_AetnaMembers a

	left join tim.DimMemberAttributionProviders b on a.PayorAttributedPCP_TIN=b.ProviderTIN and a.PayorAttributedPCP_NPI=b.ProviderNPI and b.ProviderTerm is null
	left join tim.DimMemberAttributionProviders c on a.PayorAttributedPCP_TIN=c.ProviderTIN and a.PayorAttributedPCP_NPI=c.ProviderNPI and c.ProviderTerm is not null


where
PayorAttributedPCP_ID is not null
and PayorAttributedPCP_NPI is not null
and PatientNominatedPCP_ID is null
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule5)

end


--***********************************************************************************************************
--PROCESS REMAINING MEMBERS
--***********************************************************************************************************

if @RunRule7 = 1 or @RunAllRules = 1 --needs correcting
begin

--Remaining members with a claim but no NPI on the claim
select * into #temp_unattributed_a from (

select distinct
ClaimPayor
,MemberID
,ProviderID
,a.ProviderTIN
,null ProviderNPI
,FirstDOS
,LastDOS
,cast(null as varchar(max)) ProviderAttributionLevel
,'Unattributed' AttributionGroup
,ProviderClaimCount
,row_number() over(partition by memberid order by LastDOS desc) ProviderSequence
,8 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 8) RuleDescription

from
tim.staged_Attribution_AetnaQualifingClaims a

where
a.ProviderNPI is null

) a 

where
ProviderSequence = 1
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule5)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule6)


--Remaining members with a PNP TIN or PNP NPI in enrollment file
select distinct
'AETNA' ClaimPayor
,MemberID
,PatientNominatedPCP_ID PayorID
,PatientNominatedPCP_TIN PayorTIN
,PatientNominatedPCP_NPI PayorNPI
,cast(null as date) FirstDOS
,cast(null as date) LastDOS
,cast(null as varchar(max)) ProviderAttributionLevel
,'Unattributed' AttributionGroup
,cast(null as int) ProviderClaimCount
,cast(null as int) ProviderSequence
,8 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 8) RuleDescription

into #temp_unattributed_b

from
tim.staged_Attribution_AetnaMembers a

where
(PatientNominatedPCP_TIN is not null or PatientNominatedPCP_NPI is not null)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule5)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule6)
and MemberID not in (select MemberID from #temp_unattributed_a)


--Remaining members with either a PAP TIN or a PAP NPI in enrollment file
select distinct
'AETNA' ClaimPayor
,MemberID
,PayorAttributedPCP_ID PayorID
,PayorAttributedPCP_TIN PayorTIN
,PayorAttributedPCP_NPI PayorNPI
,cast(null as date) FirstDOS
,cast(null as date) LastDOS
,cast(null as varchar(max)) ProviderAttributionLevel
,'Unattributed' AttributionGroup
,0 ProviderClaimCount
,null ProviderSequence
,8 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 8) RuleDescription

into #temp_unattributed_c

from
tim.staged_Attribution_AetnaMembers

where
(PayorAttributedPCP_TIN is not null or PayorAttributedPCP_NPI is not null)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule5)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule6)
and MemberID not in (select MemberID from #temp_unattributed_a)
and MemberID not in (select MemberID from #temp_unattributed_b)


--Remaining members with no PNP or PAP data
select distinct
'AETNA' ClaimPayor
,MemberID
,coalesce(PatientNominatedPCP_ID,PayorAttributedPCP_ID) PayorID
,coalesce(PatientNominatedPCP_TIN,PayorAttributedPCP_TIN) PayorTIN
,coalesce(PatientNominatedPCP_NPI,PayorAttributedPCP_NPI) PayorNPI
,cast(null as date) FirstDOS
,cast(null as date) LastDOS
,cast(null as varchar(max)) ProviderAttributionLevel
,'Unattributed' AttributionGroup
,cast(null as int) ProviderClaimCount
,cast(null as int) ProviderSequence
,8 RuleID
,(select RuleDescription from tim.DimMemberAttributionRules where RuleID = 8) RuleDescription

into #temp_unattributed_d

from
tim.staged_Attribution_AetnaMembers

where
MemberID not in (select MemberID from tim.staged_Attribution_Rule1)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule2)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule3)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule4)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule5)
and MemberID not in (select MemberID from tim.staged_Attribution_Rule6)
and MemberID not in (select MemberID from #temp_unattributed_a)
and MemberID not in (select MemberID from #temp_unattributed_b)
and MemberID not in (select MemberID from #temp_unattributed_c)


--Combine unattributed members

drop table tim.staged_Attribution_RuleUnattributed

select * into tim.staged_Attribution_RuleUnattributed

from (

		  select * from #temp_unattributed_a
	union select * from #temp_unattributed_b
	union select * from #temp_unattributed_c
	union select * from #temp_unattributed_d

	) a

drop table #temp_unattributed_a, #temp_unattributed_b, #temp_unattributed_c, #temp_unattributed_d


--###########################################################################################################
--ASSEMBLE ATTRIBUTION TABLES
--###########################################################################################################

drop table tim.staged_Attribution

select *  into tim.staged_Attribution from (

select * from tim.staged_Attribution_Rule1
union select * from tim.staged_Attribution_Rule2
union select * from tim.staged_Attribution_Rule3
union select * from tim.staged_Attribution_Rule4
union select * from tim.staged_Attribution_Rule5
union select * from tim.staged_Attribution_Rule6
union select * from tim.staged_Attribution_RuleUnattributed

) a


--###########################################################################################################
-- PROCESS RULE 7 AND UNATTRIBUTED
--###########################################################################################################

--Target all records with Inactive providers. Try and set AttributionGroup to an active CIN TIN group, else set to Out of Network.
update tim.staged_Attribution 
	set 
		AttributionGroup = coalesce(
			(
			--Match active group TIN to inactive providers
			select distinct b.ProviderGroupAbbreviation 
			from 
				(
				--Unique list of TINs and their Group Abbreviations
				select distinct	ProviderTIN, ProviderGroupAbbreviation
				from tim.DimMemberAttributionProviders
				where ProviderTerm is null
				) b 

			where tim.staged_Attribution.ProviderTIN = b.ProviderTIN
			)
			, 'Out of Network'
		)

	where ProviderAttributionLevel = 'Inactive'

--If a CIN TIN was assigned in last step, set to Rule 7 (provider is inactive with an active TIN. Rule could previously been 4, 5, or 6.
update tim.staged_Attribution 
	set
		RuleID = 7
		,RuleDescription = (select RuleDescription from tim.DimMemberAttributionRules where RuleID = 7)

	where 
		ProviderAttributionLevel = 'Inactive' 
		and AttributionGroup <> 'Out of Network'


--Target all records with an 'Unattributed' AttributionGroup (these would be in Rule 8). Try and set AttributionGroup to a CIN group, else retain 'Unattributed' AttributionGroup.
update tim.staged_Attribution
	set 
		AttributionGroup = coalesce(
			(
			select distinct b.ProviderGroupAbbreviation 
			from tim.DimMemberAttributionProviders b 
			where tim.staged_Attribution.ProviderTIN = b.ProviderTIN
			)
			,'Unattributed'
		)

	where 
		AttributionGroup = 'Unattributed'


update tim.staged_Attribution
	set 
		RuleID = 7, 
		RuleDescription = (select RuleDescription from tim.DimMemberAttributionRules where RuleID = 7)

	where
		RuleID = 8
		and ProviderAttributionLevel is null
		and AttributionGroup <> 'Unattributed'


--###########################################################################################################
--ASSEMBLE FINAL ATTRIBUTION TABLE
--###########################################################################################################
--Put final output into DimMemberAttribution

drop table tim.DimMemberAttribution

--truncate table tim.DimMemberAttribution
--insert into tim.DimMemberAttribution

select distinct
'AETNA' Payor
,a.MemberID
,a.PatientFirstName
,a.PatientMiddleName
,a.PatientLastName
,a.PatientGender
,a.PatientDOB
,b.RuleID
,b.RuleDescription
,b.ProviderAttributionLevel
,b.AttributionGroup
,b.ProviderID
,b.ProviderTIN
,b.ProviderNPI
,b.FirstDOS
,b.LastDOS
,b.ProviderClaimCount
,c.ProviderName
,c.ProviderGroupAbbreviation
,c.ProviderGroup
,c.ProviderRole
,c.ProviderSpecialty
,c.ProviderStart
,c.ProviderType

into tim.DimMemberAttribution

from
tim.staged_Attribution_AetnaMembers a

	left join tim.staged_Attribution b on a.MemberID=b.MemberID
	left join tim.DimMemberAttributionProviders c on b.ProviderTIN=c.ProviderTIN and b.ProviderNPI=c.ProviderNPI and c.ProviderTerm is null

order by
b.RuleID

--**************************************************************************************

select * from tim.DimMemberAttribution order by RuleID, AttributionGroup, MemberID
select * from tim.AttributionbyGroupbyMonth 

end

--###########################################################################################################
--PUBLISH ATTRIBUTION OUTPUT TO tim.FactAttribution AND UPDATE SUMMARY TABLE tim.AttribtionbyGroupbyMonth
--###########################################################################################################


--PUBLISH to tim.FactAttribution. 
--If data already exists for this payor and month, then do nothing. 
--If you are replacing existing data, you will need to run the Overwrite process to intentionally overwrite existing data with the current output.
if @Publish = 1
begin

set @Overwrite = 0  --To protect against running both publish and overwrite processes.

if not exists (select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth) --Check if the data is already there. If so, do nothing.
begin

	--Publish Attribution results to tim.FactAttribution
	insert into tim.FactAttribution
	select 
	*
	,@EffectiveMonth
	,cast(year(@EffectiveMonth) as varchar(4)) + right('00' + cast(month(@EffectiveMonth) as varchar(2)),2) EffectiveMonthInt
	,@LoadDtm 
		
	from tim.DimMemberAttribution order by AttributionGroup, ProviderNPI, MemberID

	exec tim.Update_AttributionbyGroupbyMonth --Update Summary table
	 
end

--Report Results
select distinct
Result = case when @LoadDtm = LoadDtm then 'Publish Success' else 'Publish Failure. Data Already Exists.' end
,Rows_Entered = case when @LoadDtm = LoadDtm then (select count(MemberID) from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth) else 0 end
,LoadDtm ExistingLoadTime
,@LoadDtm CurrentLoadTime
	
from tim.FactAttribution
where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth

--Display output from tim.FactAttribution
select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth   --Show detail
select * from tim.AttributionbyGroupbyMonth where Payor = 'AETNA' and Month = @EffectiveMonth order by AttributionGroup --Show summary

end

--###########################################################################################################
--OVERWRITE PROCESS tim.FactAttribution AND UPDATE SUMMARY TABLE tim.AttribtionbyGroupbyMonth
--###########################################################################################################

--OVERWRITE existing data in tim.FactAttribution matching @EffectiveMonth and Payor = 'AETNA'
if @Overwrite = 1 and @Publish = 0 --Overwrite data in tim.FactAttribution with current output. Do not run overwrite if we are currently publishing  
begin

--If matching data exists, delete matching data and insert attribution output from this script. If no data exists in destination table, do nothing.
if exists (select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth)
begin

	delete from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth

	insert into tim.FactAttribution
	select 
	*
	,@EffectiveMonth
	,cast(year(@EffectiveMonth) as varchar(4)) + right('00' + cast(month(@EffectiveMonth) as varchar(2)),2) EffectiveMonthInt
	,@LoadDtm 
		
	from tim.DimMemberAttribution order by RuleID, AttributionGroup, ProviderNPI, MemberID

	exec tim.Update_AttributionbyGroupbyMonth

end

--If data does not exist, report overwrite failure.
if not exists (select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth)
begin

	select
	'Overwrite Failure. No Data Exists. Run PUBLISH process instead.' Result
	,0 Rows_Entered

end

--If overwrite was successfull, report success and number of rows inserted.
if exists (select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth)
begin

select distinct
'Overwrite Success' Result
,Rows_Entered = case when @LoadDtm = LoadDtm then (select count(MemberID) from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth) else 0 end
,LoadDtm ExistingLoadTime
,@LoadDtm CurrentLoadTime

from tim.FactAttribution
where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth

exec tim.Update_AttributionbyGroupbyMonth --Update tim.AttributionbyGroupbyMonth

select * from tim.FactAttribution where Payor = 'AETNA' and EffectiveMonth = @EffectiveMonth --Show detail
select * from tim.AttributionbyGroupbyMonth where Payor = 'AETNA' and Month = @EffectiveMonth order by AttributionGroup	--Show summary

end

end
--20,955
--0:23